import jwt from "jsonwebtoken";
import { ObjectId, Types } from "mongoose";

const getRefresherToken = (id: Types.ObjectId) => {
  return jwt.sign({ userId: id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: "30d",
  });
};
const getAccessToken = (id: Types.ObjectId) => {
  return jwt.sign({ userId: id }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "30m",
  });
};
export { getRefresherToken, getAccessToken };
