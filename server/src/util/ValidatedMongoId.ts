import mongoose from "mongoose";

const validatedMongoId = (id: any) => {
  const isValid = mongoose.Types.ObjectId.isValid(id);
  if (!isValid) throw new Error("This id is not valid or not Found");
};

export default validatedMongoId;
