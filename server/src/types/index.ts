import { ObjectId } from "mongoose";

export interface CartType {
  _id: ObjectId | String;
  count: number;
  price: number;
}
