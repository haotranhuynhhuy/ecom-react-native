import express from "express";
import { isAdmin, verifyToken } from "../middleware/auth";
import Order from "../model/Order";
import User from "../model/User";
import Cart from "../model/Cart";
import uniqid from "uniqid";
import Product from "../model/Product";

const router = express.Router();

// @route GET api/order
// @desc Get order of user
// @access Public
router.get("/", verifyToken, async (req: any, res) => {
  try {
    const { userId } = req;
    const order = await Order.findOne({ orderby: userId })
      .populate("products.product")
      .populate("orderby")
      .exec();
    res.status(200).json({
      success: true,
      order,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/order/all-orders
// @desc Get all order of user in admin mode
// @access Public
router.get("/all", verifyToken, isAdmin, async (req, res) => {
  try {
    const allOrders = await Order.find()
      .populate("products.product")
      .populate("orderby")
      .exec();
    res.status(200).json({
      success: true,
      allOrders,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/order/:id
// @desc Get all order of user in admin mode
// @access Public
router.get("/all/:id", verifyToken, isAdmin, async (req: any, res) => {
  try {
    const { id } = req.params;
    const userOrder = await Order.findOne({ orderby: id })
      .populate("products.product")
      .populate("orderby")
      .exec();
    res.status(200).json({
      success: true,
      userOrder,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route PUT api/order/update-order/:id
// @desc PUT all order of user in admin mode
// @access Public
router.get(
  "/update-order/:id",
  verifyToken,
  isAdmin,
  async (req: any, res: any) => {
    try {
      const { id } = req.params;
      const { status } = res.body;

      const updateStatusOrder = await Order.findByIdAndUpdate(id, {
        orderStatus: status,
        paymentIntent: {
          status: status,
        },
      });

      res.status(200).json({
        success: true,
        updateStatusOrder,
      });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ success: false, message: "Internal server error" });
    }
  }
);

// @route Post api/order
// @desc Post create an order
// @access Public
router.post("/", verifyToken, async (req: any, res) => {
  try {
    const { COD } = req.body;
    const { userId } = req;
    if (!COD) throw new Error("Create cash order failed");
    const user = await User.findById(userId);
    const userCart = await Cart.findOne({ orderby: user._id });
    await Order.create({
      products: userCart.products,
      paymentIntent: {
        id: uniqid(),
        method: "COD",
        amount: userCart.cartTotal,
        status: "Cash on Delivery",
        created: Date.now(),
        currency: "vnd",
      },
      orderby: user._id,
      orderStatus: "Cash on Delivery",
    });

    //Cập nhật lại số lượng sản phẩm cho kho
    const updateStore = userCart.products.map((item) => {
      return {
        updateOne: {
          filter: { _id: item.product._id },
          update: { $inc: { quantity: -item.count, sold: +item.count } },
        },
      };
    });
    // Cho phép cập nhật hàng loạt và liên tục
    await Product.bulkWrite(updateStore, {});
    res
      .status(200)
      .json({ success: true, message: "Order is created successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

export default router;
