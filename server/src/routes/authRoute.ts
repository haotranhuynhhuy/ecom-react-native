import express from "express";
import User from "../model/User";
import argon2 from "argon2";
import jwt from "jsonwebtoken";
import { verifyRefreshToken, verifyToken } from "../middleware/auth";
import { getAccessToken, getRefresherToken } from "../config/getToken";

const router = express.Router();
// @route POST api/auth/user
// @desc Get user
// @access Public
router.get("/", verifyToken, async (req: any, res) => {
  try {
    const user = await User.findById(req.userId).select("-password");
    if (!user) {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
    res.status(200).json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POST api/auth/register
// @desc Register user
// @access Public
router.post("/register", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });

    if (user) {
      return res.status(402).json({
        success: false,
        message: "Email already exists",
      });
    }

    const hashedPassword = await argon2.hash(password);
    await User.create({
      ...req.body,
      password: hashedPassword,
    });

    res.status(200).json({
      success: true,
      message: "User logged in successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POST api/auth/login
// @desc Login user
// @access Public
router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(402).json({
        success: false,
        message: "Incorrect username or password",
      });
    }

    const passwordValid = await argon2.verify(user.password, password);
    if (!passwordValid) {
      return res.status(402).json({
        success: false,
        message: "Incorrect username or password",
      });
    }

    const accessToken = await getAccessToken(user._id);
    const refreshToken = await getRefresherToken(user._id);

    res.status(200).json({
      success: true,
      message: "User logged in successfully",
      accessToken,
      refreshToken,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});
// @route POST api/auth/admin/login
// @desc Login admin
// @access Public
router.post("/admin/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (user.role !== "admin") throw new Error("401 Not Authorized");
    if (!user) {
      return res.status(402).json({
        success: false,
        message: "Incorrect username or password",
      });
    }
    const passwordValid = await argon2.verify(user.password, password);
    if (!passwordValid) {
      return res.status(402).json({
        success: false,
        message: "Incorrect username or password",
      });
    }

    const accessToken = await getAccessToken(user._id);
    const refreshToken = await getRefresherToken(user._id);

    res.status(200).json({
      success: true,
      message: "User logged in successfully",
      accessToken,
      refreshToken,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POST api/auth/refresh-token
// @desc Post generate new access-token
// @access Public
router.post("/refresh-token", verifyRefreshToken, async (req: any, res) => {
  try {
    const { userId } = req;
    const user = await User.findById(userId).select("-password");
    if (!user) {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }

    const accessToken = await getAccessToken(user._id);
    res.status(200).json({
      success: true,
      accessToken,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});
export default router;
