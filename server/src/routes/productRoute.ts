import express from "express";
import { isAdmin, verifyToken } from "../middleware/auth";
import Product from "../model/Product";
import validatedMongoId from "../util/ValidatedMongoId";

const router = express.Router();

// @route POST api/product/
// @desc Create product
// @access Public
router.post("/", verifyToken, isAdmin, async (req, res) => {
  try {
    const newProduct = await Product.create(req.body);
    res.status(200).json({
      success: true,
      message: "Product is created successfully",
      newProduct,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route DELETE api/product/:id
// @desc Delete product
// @access Public
router.delete("/:id", verifyToken, isAdmin, async (req, res) => {
  try {
    const id = req.params;
    const deleteProduct = await Product.findByIdAndDelete(validatedMongoId(id));
    res.status(200).json({
      success: true,
      message: "Product is deleted successfully",
      deleteProduct,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route UPDATE api/product/:id
// @desc Update product
// @access Public
router.put("/update/:id", verifyToken, isAdmin, async (req, res) => {
  try {
    const id = req.params;
    const updateProduct = await Product.findOneAndUpdate({ id }, req.body);
    res.status(200).json({
      success: true,
      message: "Product is updated successfully",
      updateProduct,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/product/:id
// @desc Get single product
// @access Public
router.get("/:id", verifyToken, async (req, res) => {
  try {
    const id = req.params;
    const product = await Product.findOne({ id });
    res.status(200).json({
      success: true,
      product,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/product/
// @desc Get all product
// @access Public
router.get("/", async (req, res) => {
  try {
    //Filter query
    const queryObj = { ...req.query };
    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

    let query = Product.find(JSON.parse(queryStr));

    // Sorting

    if (typeof req.query.sort === "string") {
      const sortBy = req.query.sort.split(",").join(" ");
      query = query.sort(sortBy);
    } else {
      query = query.sort("-createdAt");
    }

    // pagination

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 10;

    const startIndex = (page - 1) * limit;
    query = query.skip(startIndex).limit(limit);

    if (req.query.page) {
      const productCount = await Product.countDocuments();
      if (startIndex >= productCount)
        throw new Error("This Page does not exists");
    }

    const product = await query;

    res.status(200).json({
      success: true,
      product,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route PUT api/product/rating
// @desc Update rating product
// @access Public
router.put("/rating", verifyToken, async (req: any, res) => {
  try {
    const { userId } = req;
    const { star, prodId, comment } = req.body;
    const product = await Product.findById(prodId);

    let alreadyRated = product.ratings.find(
      (_id) => _id.postedby.toString() === userId
    );

    if (alreadyRated) {
      await Product.updateOne(
        {
          ratings: { $elemMatch: alreadyRated },
        },
        {
          $set: { "ratings.$.star": star, "ratings.$.comment": comment },
        }
      );
    } else {
      await Product.findByIdAndUpdate(prodId, {
        $push: {
          ratings: {
            star: star,
            comment: comment,
            postedby: userId,
          },
        },
      });
    }
    const getAllRatings = await Product.findById(prodId);
    res.status(200).json({
      success: true,
      getAllRatings,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});
export default router;
