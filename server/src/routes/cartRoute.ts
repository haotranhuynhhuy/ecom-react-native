import express from "express";
import { verifyToken } from "../middleware/auth";
import validatedMongoId from "../util/ValidatedMongoId";
import Cart from "../model/Cart";
import User from "../model/User";
import Product from "../model/Product";
import { CartType } from "../types";

const router = express.Router();

// @route GET api/cart
// @desc Get cart of user
// @access Public
router.get("/", verifyToken, async (req: any, res) => {
  try {
    const { userId } = req;
    const cart = await Cart.findOne({ orderby: userId }).populate(
      "products.product"
    );
    res.status(200).json({
      success: true,
      cart,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POst api/cart
// @desc Post add cart
// @access Public
router.post("/", verifyToken, async (req: any, res) => {
  try {
    const { cart } = req.body;
    const { userId } = req;
    const user = await User.findById(userId);

    // check if user already have product in cart
    const alreadyExistCart = await Cart.findOne({ orderby: user._id });
    if (alreadyExistCart) {
      // Nếu giỏ hàng đã tồn tại, chỉ cập nhật hoặc thêm sản phẩm vào giỏ hàng hiện tại
      for (const cartItem of cart) {
        const product = await Product.findById(cartItem._id)
          .select("price")
          .exec();

        const existingProduct = alreadyExistCart.products.find(
          (item: any) => item.product.toString() === cartItem._id
        );

        if (existingProduct) {
          // Nếu sản phẩm đã tồn tại trong giỏ hàng, cập nhật số lượng và giá
          existingProduct.count = cartItem.count;
          existingProduct.price = product.price;
        } else {
          // Nếu sản phẩm chưa tồn tại trong giỏ hàng, thêm mới sản phẩm
          alreadyExistCart.products.push({
            product: cartItem._id,
            count: cartItem.count,
            price: product.price,
          });
        }
      }

      // Cập nhật lại tổng giá trị giỏ hàng
      alreadyExistCart.cartTotal = alreadyExistCart.products.reduce(
        (total, item: any) => total + item.price * item.count,
        0
      );

      await alreadyExistCart.save();
      res.status(200).json({
        success: true,
        cart: alreadyExistCart,
      });
    } else {
      // Nếu giỏ hàng không tồn tại, tạo giỏ hàng mới
      const products = await Promise.all(
        cart.map(async (cartItem: CartType) => {
          const product = await Product.findById(cartItem._id)
            .select("price")
            .exec();

          return {
            product: cartItem._id,
            count: cartItem.count,
            price: product.price,
          };
        })
      );

      const cartTotal = products.reduce(
        (total, item: CartType) => total + item.price * item.count,
        0
      );

      const newCart = await Cart.create({
        products,
        cartTotal,
        orderby: userId,
      });

      res.status(200).json({
        success: true,
        cart: newCart,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route DELETE api/cart/empty-card
// @desc Delete cart of user
// @access Public
router.delete("/empty-cart", verifyToken, async (req: any, res) => {
  try {
    const user = await User.findOne({ _id: req.userId });
    await Cart.findOneAndRemove({ orderby: user._id });
    res.status(200).json({
      success: true,
      message: "Cart is deleted successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});
export default router;
