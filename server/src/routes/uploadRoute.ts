import express from "express";
import { isAdmin, verifyToken } from "../middleware/auth";
import { productImgResize, uploadPhoto } from "../middleware/uploadImage";
import { cloudinaryDeleteImg, cloudinaryUploadImg } from "../util/cloudinary";
import fs from "fs";
const router = express.Router();
// @route Post api/upload
// @desc Post upload image product
// @access Public
router.post(
  "/",
  verifyToken,
  isAdmin,
  uploadPhoto.array("images", 10),
  productImgResize,
  async (req: any, res) => {
    try {
      const uploader = (path) => cloudinaryUploadImg(path);
      const files = req.files;
      const uploadPromises = files.map(async (file) => {
        const { path } = file;
        const newpath = await uploader(path);
        fs.unlinkSync(path);
        return newpath;
      });
      const urls = await Promise.all(uploadPromises);
      res.status(200).json({
        success: true,
        message: "Images is uploaded successfully",
        urls,
      });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ success: false, message: "Internal server error" });
    }
  }
);

// @route Delete api/upload/:id
// @desc Delete delete image product
// @access Public
router.delete("/:id", verifyToken, isAdmin, async (req: any, res) => {
  try {
    // id ở đây sẽ là public_id trong cloudinary
    const { id } = req.params;
    const deleted = await cloudinaryDeleteImg(id);
    res.status(200).json({
      success: true,
      message: "Image is deleted successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});
export default router;
