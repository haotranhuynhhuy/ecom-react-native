import jwt from "jsonwebtoken";

const verifyToken = (req, res, next) => {
  try {
    const authHeader: String = req.header("Authorization");
    const token = authHeader && authHeader.split(" ")[1];
    if (!token) {
      return res
        .status(401)
        .json({ success: false, message: "Unauthorized Error" });
    }
    const decode = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    req.userId = decode.userId;
    next();
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server Error" });
  }
};
const isAdmin = (req, res, next) => {
  try {
    const adminHeader = req.headers["role"];
    if (adminHeader === "user" || !adminHeader) {
      return res
        .status(403)
        .json({ success: false, message: "Forbidden Error" });
    }
    next();
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server Error" });
  }
};
const verifyRefreshToken = (req, res, next) => {
  try {
    const { token } = req.body;
    const refreshToken = token && token.split(" ")[1];
    if (!refreshToken) {
      return res
        .status(401)
        .json({ success: false, message: "Unauthorized Error" });
    }
    jwt.verify(
      refreshToken,
      process.env.REFRESH_TOKEN_SECRET,
      (err, result) => {
        try {
          if (err) {
            return res
              .status(401)
              .json({ success: false, message: "Token is expired" });
          }
          req.userId = result.userId;
          return next();
        } catch (error) {
          console.log(error);
          return res
            .status(500)
            .json({ success: false, message: "Server Error" });
        }
      }
    );
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server Error" });
  }
};
export { verifyToken, isAdmin, verifyRefreshToken };
