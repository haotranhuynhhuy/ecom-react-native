package com.mycompany.mywebapp.shared;

import com.google.gwt.core.client.JavaScriptObject;

public class Images extends JavaScriptObject {
    private String public_id;
    private String url;
    private String id;

    // Constructor
    public Images(String public_id, String url, String id) {
        this.public_id = public_id;
        this.url = url;
        this.id = id;
    }

    // Getter methods
    public String getPublic_id() {
        return public_id;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    // Setter methods
    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setId(String id) {
        this.id = id;
    }
}
