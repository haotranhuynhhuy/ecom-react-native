package com.mycompany.mywebapp.shared;

import com.google.gwt.core.client.JavaScriptObject;

public class Rating extends JavaScriptObject {
    private String comment;
    private String postedby;
    private String id;
    private int star;

    // Constructor
    public Rating(String comment, String postedby, String id, int star) {
        this.comment = comment;
        this.postedby = postedby;
        this.id = id;
        this.star = star;
    }

    // Getter methods
    public String getComment() {
        return comment;
    }

    public String getPostedBy() {
        return postedby;
    }

    public String getId() {
        return id;
    }

    public int getStar() {
        return star;
    }

    // Setter methods
    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setPostedBy(String postedby) {
        this.postedby = postedby;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStar(int star) {
        this.star = star;
    }
}
