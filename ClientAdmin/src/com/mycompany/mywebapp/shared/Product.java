package com.mycompany.mywebapp.shared;

import com.google.gwt.core.client.JavaScriptObject;

public class Product extends JavaScriptObject {
    private String id;
    private String title;
    private Images[] images;
    private String description;
    private double price;
    private int quantity;
    private Rating[] ratings;
    private String totalRating;

    public Product() {
    }

    // Constructor
    public Product(String id, String title, Images[] images, String description, double price, int quantity,
            Rating[] ratings, String totalRating) {
        this.id = id;
        this.title = title;
        this.images = images;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.ratings = ratings;
        this.totalRating = totalRating;
    }

    // Getter methods
    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Images[] getImages() {
        return images;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public Rating[] getRatings() {
        return ratings;
    }

    public String getTotalRating() {
        return totalRating;
    }

    // Setter methods
    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImages(Images[] images) {
        this.images = images;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setRatings(Rating[] ratings) {
        this.ratings = ratings;
    }

    public void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }
}
