package com.mycompany.mywebapp.client.libs;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.storage.client.Storage;

public class HttpClient {
    private String baseUrl = "https://harveytran-be-ecom.onrender.com/api";
    private Config config = new Config();
    private Storage localStorage = Storage.getLocalStorageIfSupported();
    private RequestBuilder requestBuilder;

    private String queryParams(String url, int page, int limit, String sort) {
        StringBuilder fullUrlBuilder = new StringBuilder(baseUrl + url);
        fullUrlBuilder.append("?page=").append(page).append("&limit=").append(limit).append("&sort=").append(sort);
        return fullUrlBuilder.toString();
    }

    private void setAuthorizationHeader() {
        requestBuilder.setHeader("Content-Type", "application/json");
        String token = getAccessToken();
        if (token != null && !token.isEmpty()) {
            requestBuilder.setHeader("Authorization", config.oauth20.tokenType + " " + token);
        } else {
            throw new Error("401 Not Authorized");
        }
    }
    /*
     * Phương thức GET
     */

    public void GET(String url, RequestCallback callback) {
        String fullUrl = baseUrl + url;
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, fullUrl);
        try {
            requestBuilder.setHeader("Content-Type", "application/json");
            requestBuilder.sendRequest(null, callback);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public void GET(String url, int page, int limit, String sort, RequestCallback callback) {
        String fullUrl = queryParams(url, page, limit, sort);
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, fullUrl);
        setAuthorizationHeader();
        try {
            requestBuilder.sendRequest(null, callback);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    /*
     * Phương thức POST
     */

    public void POST(String url, JSONObject data, RequestCallback callback) {
        String fullUrl = baseUrl + url;
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.POST, fullUrl);
        try {
            requestBuilder.setHeader("Content-Type", "application/json");
            requestBuilder.setRequestData(data.toString());
            // Check if the data is valid JSON before sending the request
            requestBuilder.sendRequest(data.toString(), callback);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    /*
     * Phương thức PUT
     */

    public void PUT(String url, JSONObject data, RequestCallback callback) {
        String fullUrl = baseUrl + url;
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.PUT, fullUrl);
        setAuthorizationHeader();
        try {
            requestBuilder.setRequestData(data.toString());
            requestBuilder.sendRequest(data.toString(), callback);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    /*
     * Phương thức DELETE
     */
    public void DELETE(String url, RequestCallback callback) {
        String fullUrl = baseUrl + url;
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.DELETE, fullUrl);
        setAuthorizationHeader();
        try {
            requestBuilder.sendRequest(null, callback);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public String getAccessToken() {
        return localStorage.getItem(config.getAccessTokenStorageKey());
    }

    public void setAccessToken(String token) {
        localStorage.setItem(config.getAccessTokenStorageKey(), token);
    }

    public String getRefreshToken() {
        return localStorage.getItem(config.getRefreshTokenStorageKey());
    }

    public void setRefreshToken(String token) {
        localStorage.setItem(config.getRefreshTokenStorageKey(), token);
    }

    public void clearToken() {
        String[] keysToRemove = {
                config.getAccessTokenStorageKey(),
                config.getRefreshTokenStorageKey()
        };
        for (String key : keysToRemove) {
            localStorage.removeItem(key);
        }
    }

    public static class Config {
        private String accessTokenStorageKey = "access-token";
        private String refreshTokenStorageKey = "refresh-token";
        // private String refreshTokenEndpoint = "/refresh-token";
        private OAuth20 oauth20 = new OAuth20();

        public String getAccessTokenStorageKey() {
            return accessTokenStorageKey;
        }

        public String getRefreshTokenStorageKey() {
            return refreshTokenStorageKey;
        }

        // public String getRefreshTokenEndpoint() {
        // return refreshTokenEndpoint;
        // }

        public OAuth20 getOAuth20() {
            return oauth20;
        }

        public static class OAuth20 {
            private String tokenType = "Bearer";

            public String getTokenType() {
                return tokenType;
            }
        }
    }
}
