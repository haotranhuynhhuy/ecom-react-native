package com.mycompany.mywebapp.client.layout.DefaultLayout;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

public class DefaultLayout extends Composite {
    interface DefaultLayoutUiBinder extends UiBinder<HTMLPanel, DefaultLayout> {
    }

    private static DefaultLayoutUiBinder uiBinder = GWT.create(DefaultLayoutUiBinder.class);

    @UiField
    HTMLPanel headerUI;

    @UiField
    HTMLPanel sidebarUI;

    @UiField
    HTMLPanel content;

    public DefaultLayout() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    public HTMLPanel getHeader() {
        return headerUI;
    }

    public HTMLPanel getSidebar() {
        return sidebarUI;
    }

    public HTMLPanel getContent() {
        return content;
    }

}
