package com.mycompany.mywebapp.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.RootPanel;
import com.mycompany.mywebapp.client.layout.DefaultLayout.DefaultLayout;
import com.mycompany.mywebapp.client.ui.Authen.Login.Login;
import com.mycompany.mywebapp.client.ui.Header.Header;
import com.mycompany.mywebapp.client.ui.Sidebar.Sidebar;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ClientAdmin implements EntryPoint {
  public void onModuleLoad() {

    String token = checkToken();

    if (token != null && !token.isEmpty()) {
      navigateToHomepage();
    } else {
      navigateToLogin();
    }
  }

  public String checkToken() {
    Storage localStorage = Storage.getLocalStorageIfSupported();
    String token = localStorage.getItem("access-token");
    return token;
  }

  private void navigateToHomepage() {
    RootPanel.get().clear();
    final DefaultLayout defaultLayout = new DefaultLayout();
    Header headerUI = new Header();
    defaultLayout.getHeader().add(headerUI);

    Sidebar sidebarUI = new Sidebar();
    defaultLayout.getSidebar().add(sidebarUI);

    // Home home = new Home();
    // defaultLayout.getContent().add(home);

    RootPanel.get().add(defaultLayout);
    History.newItem("homepage");
  }

  private void navigateToLogin() {
    Login login = new Login();
    RootPanel.get().clear();
    RootPanel.get().add(login);
    History.newItem("login");
  }
}
