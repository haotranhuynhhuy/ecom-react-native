package com.mycompany.mywebapp.client.ui.Sidebar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.mycompany.mywebapp.client.ui.Authen.Login.Login;

public class Sidebar extends Composite {
    interface SidebarUiBinder extends UiBinder<HTMLPanel, Sidebar> {
    }

    private static SidebarUiBinder uiBinder = GWT.create(SidebarUiBinder.class);

    @UiField
    Label logout;

    public Sidebar() {
        initWidget(uiBinder.createAndBindUi(this));
        logout.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onLogout();
            }

        });
    }

    public void onLogout() {
        Storage lStorage = Storage.getLocalStorageIfSupported();
        lStorage.removeItem("access-token");
        Login login = new Login();
        RootPanel.get().clear();
        RootPanel.get().add(login);
        History.newItem("login");
    }
}
