package com.mycompany.mywebapp.client.ui.Authen.Login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.mycompany.mywebapp.client.layout.DefaultLayout.DefaultLayout;
import com.mycompany.mywebapp.client.libs.HttpClient;
import com.mycompany.mywebapp.client.ui.Header.Header;
import com.mycompany.mywebapp.client.ui.Sidebar.Sidebar;

public class Login extends Composite {
    interface LoginUiBinder extends UiBinder<HTMLPanel, Login> {
    }

    private static LoginUiBinder ourUiBinder = GWT.create(LoginUiBinder.class);

    @UiField
    Label errorMessage;

    @UiField
    TextBox emailTextBox;

    @UiField
    PasswordTextBox passwordTextBox;

    @UiField
    Button submitButton;

    private HttpClient httpClient = new HttpClient();

    public Login() {
        initWidget(ourUiBinder.createAndBindUi(this));
        emailTextBox.removeStyleName("gwt-TextBox");
        emailTextBox.getElement().setAttribute("id", "email");
        emailTextBox.getElement().setAttribute("placeholder", "Enter your email");
        passwordTextBox.removeStyleName("gwt-PasswordTextBox");
        passwordTextBox.getElement().setAttribute("id", "password");
        passwordTextBox.getElement().setAttribute("placeholder", "Enter your password");

        submitButton.removeStyleName("gwt-Button");

        submitButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                login();
            }

        });

        // keyboard event
        passwordTextBox.addKeyDownHandler(new KeyDownHandler() {
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    login();
                }
            }
        });
        // Create focus username or pass to delete error message
        FocusHandler focusHandler = new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                errorMessage.setText("");
            }

        };
        emailTextBox.addFocusHandler(focusHandler);
        passwordTextBox.addFocusHandler(focusHandler);

    }

    public void login() {
        String email = emailTextBox.getValue();
        String password = passwordTextBox.getValue();

        JSONObject requestData = new JSONObject();
        requestData.put("email", new JSONString(email));
        requestData.put("password", new JSONString(password));

        RequestCallback callback = new RequestCallback() {

            @Override
            public void onResponseReceived(Request request, Response response) {
                if (response.getStatusCode() == 200) {
                    // Xử lý phản hồi thành công từ máy chủ
                    JSONValue jsonValue = JSONParser.parseStrict(response.getText());
                    JSONObject jsonObject = jsonValue.isObject();
                    String accessToken = jsonObject.get("accessToken").isString().stringValue();
                    String fresherToken = jsonObject.get("refreshToken").isString().stringValue();
                    httpClient.setAccessToken(accessToken);
                    httpClient.setRefreshToken(fresherToken);
                    navigateToHomepage();
                } else {
                    errorMessage.setText("Email or password incorrect");
                }
            }

            @Override
            public void onError(Request request, Throwable exception) {
                // TODO Auto-generated method stub
                throw new UnsupportedOperationException("Unimplemented method 'onError'");
            }

        };
        httpClient.POST("/auth/admin/login", requestData, callback);
    }

    private void navigateToHomepage() {
        RootPanel.get().clear();
        final DefaultLayout defaultLayout = new DefaultLayout();
        Header headerUI = new Header();
        defaultLayout.getHeader().add(headerUI);

        Sidebar sidebarUI = new Sidebar();
        defaultLayout.getSidebar().add(sidebarUI);

        // Home home = new Home();
        // defaultLayout.getContent().add(home);

        RootPanel.get().add(defaultLayout);
        History.newItem("homepage");
    }

}
